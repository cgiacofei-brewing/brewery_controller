# Changelog

## [1.0.0] - 2022-01-14
First working release running in the brewery.

### Features
Super simple rotary encoder control.
 - Rotary button toggles heat on/off.
 - Turning encoder raises and lowers element power (duty cycle).
 - rotary speed is used to advance power faster if the knob is turned faster.

## [0.0.1] - 2021-09-13
Initial upload of files.

### Working
- LCD display and Rotary Encoder
- Output power can be viewed on display and modified using the uncoder.
- PWM output of the kettle IO pin.

### Untested
- MQTT comunication.
