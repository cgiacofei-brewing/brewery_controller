# Arduino Brewery Control

Simple PWM & (eventually) temperature control for an Arduino based electric brewing system.

## Libraries needed to make this thing work
 - [ArduinoJson.h](https://www.arduino.cc/reference/en/libraries/arduinojson/)
 - [MD_REncoder.h](https://www.arduino.cc/reference/en/libraries/md_rencoder/)
 - [LiquidMenu.h](https://www.arduino.cc/reference/en/libraries/liquidmenu/)
 - [LiquidCrystal_I2C.h](https://www.arduino.cc/reference/en/libraries/liquidcrystal-i2c/)
 - [MQTT.h](https://www.arduino.cc/reference/en/libraries/mqtt/)
 - [Adafruit_MAX31865.h](https://www.arduino.cc/reference/en/libraries/adafruit-max31865-library/)
