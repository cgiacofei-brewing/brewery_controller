# Temperature Control

Enables basic thermostat control for fermentation, kegerator, etc.

## Requirements
Arduino Nano (clone) with ethernet shield.

For programing the board:
  - Board : Arduino Nano
  - Processor : ATmega328P
  - Port : /dev/ttyUSB0

This board/shield combo works with the [EthernetENC](https://github.com/jandrassy/EthernetENC/wiki) library.
